package com.nvtien.dhbtc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Vance on 20/11/2015.
 */
public class Over extends Activity implements View.OnClickListener {

    private Button btHome, btReplay;
    private TextView tvScore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.over);
        initView();
    }

    private void initView() {
        btHome=(Button)findViewById(R.id.bt_home);
        btHome.setOnClickListener(this);
        btReplay=(Button)findViewById(R.id.bt_replay);
        btReplay.setOnClickListener(this);
        Intent getInfo = getIntent();
        String score = "Your score is: " + getInfo.getIntExtra(Main.KEY_SCORE,0);
        tvScore=(TextView)findViewById(R.id.tv_score);
        tvScore.setText(score);
    }

    @Override
    public void onClick(View v) {
        if(v==btHome){
            startActivity(new Intent(this, Home.class));
            finish();
        }
        if(v==btReplay){
            startActivity(new Intent(this, Main.class));
            finish();
        }
    }
}
