package com.nvtien.dhbtc;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Vance on 15/11/2015.
 */
public class QuestionList {
    /**A class contains data about questions*/
    public ArrayList<Question> questions = new ArrayList<>();
    public ArrayList<Question> sortedQuestions = new ArrayList<>();

    public QuestionList(){
        initData();
        randomList();
    }

    /**Random the order of the questions list*/
    private void randomList() {
        boolean[] randomQuestion = new boolean[sortedQuestions.size()];
        for(int i=0; i<sortedQuestions.size(); i++){
            randomQuestion[i]=false;
        }
        Random r = new Random();
        int j =0;
        while (j<sortedQuestions.size()){
            int index = r.nextInt(sortedQuestions.size());
            if(!randomQuestion[index]){
                Question tmpQuestion = new Question(sortedQuestions.get(index).getPicture(),
                        sortedQuestions.get(index).getResult());
                questions.add(tmpQuestion);
                randomQuestion[index]=true;
                j++;
            }
        }

    }

    /**Put data of questions into the class in an order*/
    private void initData() {
        addQuestion(R.drawable.aomua,"aomua");
        addQuestion(R.drawable.baocao,"baocao");
        addQuestion(R.drawable.canthiep,"canthiep");
        addQuestion(R.drawable.cattuong,"cattuong");
        addQuestion(R.drawable.chieutre,"chieutre");
        addQuestion(R.drawable.danhlua,"danhlua");
        addQuestion(R.drawable.danong,"danong");
        addQuestion(R.drawable.giandiep,"giandiep");
        addQuestion(R.drawable.giangmai,"giangmai");
        addQuestion(R.drawable.hoidong,"hoidong");
        addQuestion(R.drawable.hongtam,"hongtam");
        addQuestion(R.drawable.khoailang,"khoailang");
        addQuestion(R.drawable.kiemchuyen,"kiemchuyen");
        addQuestion(R.drawable.lancan,"lanccan");
        addQuestion(R.drawable.masat,"masat");
        addQuestion(R.drawable.nambancau,"nambancau");
        addQuestion(R.drawable.oto,"oto");
        addQuestion(R.drawable.quyhang,"quyhang");
        addQuestion(R.drawable.songsong,"songsong");
        addQuestion(R.drawable.thattinh,"thattinh");
        addQuestion(R.drawable.thothe,"thothe");
        addQuestion(R.drawable.tichphan,"tichphan");
        addQuestion(R.drawable.tohoai,"tohoai");
        addQuestion(R.drawable.totien,"totien");
        addQuestion(R.drawable.tranhthu,"tranhthu");
        addQuestion(R.drawable.vuaphaluoi,"vuaphaluoi");
        addQuestion(R.drawable.vuonbachthu,"vuonbachthu");
        addQuestion(R.drawable.xakep,"xakep");
        addQuestion(R.drawable.xaphong,"xaphong");
        addQuestion(R.drawable.xedapdien,"xedapdien");
    }

    private void addQuestion(int pictureId, String result) {
        /**Add data of one question to the list*/
        Question tmpQuestion = new Question(pictureId, result);
        sortedQuestions.add(tmpQuestion);
    }

    public int getPicture(int i){
        return questions.get(i).getPicture();
    }

    public String getResult(int i){
        return questions.get(i).getResult();
    }

    public int getSize(){
        return questions.size();
    }
}
