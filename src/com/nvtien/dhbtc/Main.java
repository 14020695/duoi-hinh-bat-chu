package com.nvtien.dhbtc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

/**
 * Created by Vance on 15/11/2015.
 */
public class Main extends Activity implements View.OnClickListener {

    public static final String KEY_SCORE = "score";
    private String answer, result;
    private int heart, coin, letterChosen;
    private TextView tvHeart, tvCoin, tvResult;
    private Button btNext;
    private ImageView ivQuestion;
    private Button[] btLetter, btAnswer;
    private QuestionList questionList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        heart=5;
        coin=0;
        questionList = new QuestionList();
        initView();
    }

    private void initView() {
        tvHeart=(TextView)findViewById(R.id.tv_heart);
        tvHeart.setText(""+heart);
        tvCoin=(TextView)findViewById(R.id.tv_coin);
        tvCoin.setText(""+coin);
        ivQuestion=(ImageView)findViewById(R.id.iv_question);
        ivQuestion.setImageDrawable(this.getResources().getDrawable(questionList.getPicture(coin/100)));
        tvResult=(TextView)findViewById(R.id.tv_result);
        btNext=(Button)findViewById(R.id.bt_next);
        btNext.setOnClickListener(this);
        result = questionList.getResult(coin / 100);
        initButtonLetter();
        initButtonAnswer();
        answer="";
        letterChosen=0;
    }

    /**Set the letters needed for the answer*/
    private void initButtonAnswer() {
        btAnswer=new Button[16];
        for(int i=0; i<result.length(); i++){
            btAnswer[i]=(Button)findViewById(R.id.bt_answer_1+i);
            btAnswer[i].setVisibility(View.VISIBLE);
            btAnswer[i].setOnClickListener(this);
        }
        for(int i=result.length(); i<16; i++){
            btAnswer[i]=(Button)findViewById(R.id.bt_answer_1+i);
            btAnswer[i].setVisibility(View.GONE);
        }
    }

    /**Set the letters given for player to type in*/
    private void initButtonLetter() {
        btLetter=new Button[16];
        int j=0;
        Random r = new Random();
        boolean[] setRightLetter = new boolean[16];
        for(int i=0; i<16; i++){
            btLetter[i]=(Button)findViewById(R.id.bt_letter_1+i);
            btLetter[i].setVisibility(View.VISIBLE);
            btLetter[i].setOnClickListener(this);
            char c = (char)(r.nextInt(26)+'a');
            btLetter[i].setText(""+c);
            setRightLetter[i]=false;
        }
        while (j<result.length()){
            int index = r.nextInt(16);
            if(!setRightLetter[index]){
                btLetter[index].setText(""+result.charAt(j));
                j++;
                setRightLetter[index]=true;
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onClick(View v) {
        /**If the player got no lives left, game over*/
        if(heart<=0){
            return;
        }

        /**If the player touch the letters given*/
        for(int i=0; i<16; i++){
            if(v==btLetter[i]){
                btLetter[i].setVisibility(View.INVISIBLE);
                answer+=btLetter[i].getText().toString();
                setAnswer();
                letterChosen++;
                if(letterChosen==result.length()){
                    checkAnswer();
                }

            }
        }

        /**If the player touch the next button*/
        if(v==btNext){
            nextQuestion();
        }

        /**If the player want to change his answer and touch a letter in the answer*/
        for(int i=0; i<answer.length(); i++){
            if(v==btAnswer[i]){
                answer= answer.substring(0,i)+answer.substring(i+1);
                letterChosen--;
                addLetter(btAnswer[i].getText());
                setAnswer();
            }
        }
    }

    /**Add a letter to the answer*/
    private void addLetter(CharSequence text) {
        int i=0;
        while (btLetter[i].getVisibility() == View.VISIBLE){
            i++;
        }
        btLetter[i].setText(text);
        btLetter[i].setVisibility(View.VISIBLE);
    }

    /**Set the view so that the player can see his answer*/
    private void setAnswer() {
        for(int j=0; j<answer.length(); j++){
            btAnswer[j].setText(""+answer.charAt(j));
        }
        for(int j=answer.length();j<16; j++){
            btAnswer[j].setText("");
        }
    }

    /**After the answer got enough letters, it will check if the answer was right or wrong*/
    private void checkAnswer() {
        if(answer.equals(result)){
            /**If the answer was right*/
            btNext.setVisibility(View.VISIBLE);
            tvResult.setText(this.getResources().getString(R.string.win));
            tvResult.setVisibility(View.VISIBLE);
            coin+=100;
            tvCoin.setText(""+coin);
            for(int i=0; i<16; i++){
                btAnswer[i].setBackground(this.getResources().getDrawable(R.drawable.tile_true));
            }
        } else{
            /**If the answer was wrong*/
            tvResult.setText(this.getResources().getString(R.string.lose));
            tvResult.setVisibility(View.VISIBLE);
            heart--;
            tvHeart.setText(""+heart);
            if(heart==0){
                /**If the player got no lives left, game over*/
                Toast.makeText(this,"Game Over",Toast.LENGTH_LONG).show();
                over();
                finish();
            }
            for(int i=0; i<16; i++){
                btAnswer[i].setBackground(this.getResources().getDrawable(R.drawable.tile_false));
            }
        }
    }

    /**Set the next question*/
    private void nextQuestion() {
        if(coin/100>=questionList.getSize()){
            Toast.makeText(this,"You cleared all levels",Toast.LENGTH_SHORT).show();
            over();
        }
        for(int i=0; i<16; i++){
            btAnswer[i].setText("");
            btAnswer[i].setBackground(this.getResources().getDrawable(R.drawable.tile_hover));
        }
        btNext.setVisibility(View.INVISIBLE);
        tvResult.setVisibility(View.INVISIBLE);
        initView();
    }

    /**Game over*/
    private void over() {
        Intent intent = new Intent();
        intent.setClass(this, Over.class);
        intent.putExtra(KEY_SCORE, coin);
        startActivity(intent);
        finish();
    }
}
