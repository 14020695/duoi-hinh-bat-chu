package com.nvtien.dhbtc;

import java.util.ArrayList;

/**
 * Created by Vance on 15/11/2015.
 */
public class Question {
    public int picture;
    public String result;

    public Question(int picture, String result){
        this.picture=picture;
        this.result=result;
    }

    public int getPicture() {
        return picture;
    }

    public String getResult() {
        return result;
    }

}
