package com.nvtien.dhbtc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by Vance on 20/11/2015.
 */
public class Home extends Activity implements View.OnClickListener {

    private Button btPlay, btSound;
    private boolean soundOn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        soundOn=true;
        initView();
    }

    private void initView() {
        btPlay=(Button)findViewById(R.id.bt_play);
        btPlay.setOnClickListener(this);
        /*btSound=(Button)findViewById(R.id.bt_sound);
        btSound.setOnClickListener(this);*/
    }

    @Override
    public void onClick(View v) {
        /*if(v==btSound){
            if(soundOn){
                btSound.setBackground(this.getResources().getDrawable(R.drawable.sound_off));
                soundOn=false;
            } else {
                btSound.setBackground(this.getResources().getDrawable(R.drawable.sound_on));
                soundOn=true;
            }
        }*/

        if(v==btPlay){
            startActivity(new Intent(this, Main.class));
            finish();
        }
    }
}
